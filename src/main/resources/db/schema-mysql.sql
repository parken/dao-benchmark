/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 13/06/2018 19:07:11
*/


SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_customer
-- ----------------------------
DROP TABLE IF EXISTS `sys_customer`;
CREATE TABLE `sys_customer`  (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255)  DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB ;

-- ----------------------------
-- Records of sys_customer
-- ----------------------------
INSERT INTO `sys_customer` VALUES (1, 'a', '用户一');
INSERT INTO `sys_customer` VALUES (2, 'b', '用户二');
INSERT INTO `sys_customer` VALUES (3, 'c', '用户三');

-- ----------------------------
-- Table structure for sys_order
-- ----------------------------
DROP TABLE IF EXISTS `sys_order`;
CREATE TABLE `sys_order`  (
  `id` int(11) NOT NULL,
  `name` varchar(255)  DEFAULT NULL,
  `cust_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB ;

-- ----------------------------
-- Records of sys_order
-- ----------------------------
INSERT INTO `sys_order` VALUES (1, 'a', 1);
INSERT INTO `sys_order` VALUES (2, 'b', 1);
INSERT INTO `sys_order` VALUES (3, 'c', 2);
INSERT INTO `sys_order` VALUES (4, 'd', 2);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL,
  `code` varchar(255)  DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB ;


SET FOREIGN_KEY_CHECKS = 1;

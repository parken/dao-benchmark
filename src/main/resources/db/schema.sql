CREATE TABLE hibernate_sequence (
  next_oval int PRIMARY KEY
);

-- 常规测试
CREATE TABLE sys_user (
   id int NOT NULL auto_increment,
   code  varchar(16) DEFAULT NULL,
   PRIMARY KEY ( id )
) ;

-- orm 测试的

CREATE TABLE sys_customer (
   id int NOT NULL ,
   code  varchar(16) DEFAULT NULL,
   name  varchar(16) DEFAULT NULL,
   PRIMARY KEY ( id )
) ;

insert into  sys_customer values (1,'a','用户一');
insert into  sys_customer values (2,'b','用户二');
insert into  sys_customer values (3,'c','用户三');


CREATE TABLE sys_order (
     id int NOT NULL ,
     name  varchar(16) DEFAULT NULL,
     cust_id int ,
   PRIMARY KEY ( id )
) ;

insert into  sys_order values (1,'a',1);
insert into  sys_order values (2,'b',1);
insert into  sys_order values (3,'c',2);
insert into  sys_order values (4,'d',2);


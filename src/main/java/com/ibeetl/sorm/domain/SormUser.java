package com.ibeetl.sorm.domain;

import sf.core.DBField;
import sf.core.DBObject;

import javax.persistence.*;

@Entity()
@Table(name = "sys_user")
public class SormUser extends DBObject {
    @Id
    @GeneratedValue
    private Integer id;
    @Column()
    private String code;

    public enum Field implements DBField {
        id, code
    }

    @Override
    public boolean push(String fieldName, Object value) {
        switch (fieldName) {
            case "id":
               this.id= (Integer) value;
               break;
            case "code":
                this.code= (String) value;
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public Object pull(String fieldName) {
        switch (fieldName) {
            case "id":
                return this.id;
            case "code":
                return this.code;
            default:
                break;
        }
        return null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


}

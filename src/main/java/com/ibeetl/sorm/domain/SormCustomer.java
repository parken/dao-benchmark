package com.ibeetl.sorm.domain;

import sf.core.DBCascadeField;
import sf.core.DBField;
import sf.core.DBObject;

import javax.persistence.*;
import java.util.List;

@Entity()
@Table(name = "sys_customer")
public class SormCustomer extends DBObject {
    @Id
    private Integer id;
    @Column
    private String code;
    @Column
    private String name;
    @OneToMany(mappedBy = "customer")
    List<SormSqlOrder> orders;

    public enum Field implements DBField {
        id, code, name
    }

    public enum CascadeField implements DBCascadeField {
        orders
    }

    @Override
    public boolean push(String fieldName, Object value) {
        if ("id".equals(fieldName)) {
            this.id = Integer.class.cast(value);
        } else if ("code".equals(fieldName)) {
            this.code = String.class.cast(value);
        }else if ("orders".equals(fieldName)) {
            this.orders = (List<SormSqlOrder>) value;
        } else {
            return false;
        }
        return true;
    }

    @Override
    public Object pull(String fieldName) {
        if ("id".equals(fieldName)) {
            return this.id;
        } else if ("code".equals(fieldName)) {
            return this.code;
        } else if ("orders".equals(fieldName)) {
            return this.orders;
        } else {
            return null;
        }
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SormSqlOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<SormSqlOrder> orders) {
        this.orders = orders;
    }

}

package com.ibeetl.sorm.domain;

import sf.core.DBCascadeField;
import sf.core.DBField;
import sf.core.DBObject;

import javax.persistence.*;

@Entity()
@Table(name = "sys_order")
public class SormSqlOrder extends DBObject {
    @Id
    private Integer id;
    @Column
    private String name;
    @Column(name = "cust_id")
    private Integer custId;

    @ManyToOne
    @JoinColumn(name = "custId", referencedColumnName = "id")
    private SormCustomer customer;

    public enum Field implements DBField {
        id, name, custId
    }

    public enum CascadeField implements DBCascadeField {
        customer
    }

    @Override
    public boolean push(String fieldName, Object value) {
        if ("id".equals(fieldName)) {
            this.id = Integer.class.cast(value);
        } else if ("name".equals(fieldName)) {
            this.name = String.class.cast(value);
        }else if ("custId".equals(fieldName)) {
            this.custId = Integer.class.cast(value);
        }else if ("customer".equals(fieldName)) {
            this.customer = (SormCustomer) value;
        } else {
            return false;
        }
        return true;
    }

    @Override
    public Object pull(String fieldName) {
        if ("id".equals(fieldName)) {
            return this.id;
        } else if ("name".equals(fieldName)) {
            return this.name;
        } else if ("custId".equals(fieldName)) {
            return this.custId;
        } else if ("customer".equals(fieldName)) {
            return this.customer;
        } else {
            return null;
        }
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SormCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(SormCustomer customer) {
        this.customer = customer;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }
}

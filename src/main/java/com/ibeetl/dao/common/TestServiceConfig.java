package com.ibeetl.dao.common;

import com.ibeetl.dao.anima.service.AnimaPerformaceTestService;
import com.ibeetl.dao.beetlsql.service.BeetlSqlPerformaceTestService;
import com.ibeetl.dao.jdbc.RawJDBCPerformaceTestService;
import com.ibeetl.dao.jpa.service.JpaPerformaceTestService;
import com.ibeetl.dao.mybatis.service.MyBatisPerformanceTestService;
import com.ibeetl.dao.sorm.SormPerformaceTestService;
import com.ibeetl.dao.think.service.ThinkPerformaceTestService;
import io.github.biezhi.anima.Anima;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class TestServiceConfig {
    @Bean
    @ConditionalOnProperty(name = "test_target", havingValue = "beetlsql")
    public TestServiceInterface getBeetlSqlTestService() {
        return new BeetlSqlPerformaceTestService();
    }


    @Bean
    @ConditionalOnProperty(name = "test_target", havingValue = "jpa")
    public TestServiceInterface getJpaTestService() {
        return new JpaPerformaceTestService();
    }

    @Bean
    @ConditionalOnProperty(name = "test_target", havingValue = "mybatis")
    public TestServiceInterface getMyBatisTestService() {
        return new MyBatisPerformanceTestService();
    }
    
    @Bean
    @ConditionalOnProperty(name = "test_target", havingValue = "jdbc")
    public TestServiceInterface getRawJDBCTestService() {
        return new RawJDBCPerformaceTestService();
    }

    @Bean
    @ConditionalOnProperty(name = "test_target", havingValue = "think")
    public TestServiceInterface getThinkTestService() {
        return new ThinkPerformaceTestService();
    }

    @Bean
    @ConditionalOnProperty(name = "test_target", havingValue = "anima")
    public TestServiceInterface getAnimaTestService(@Autowired DataSource dataSource) {
        Anima.open(dataSource);
        return new AnimaPerformaceTestService();
    }

	@Bean
    @ConditionalOnProperty(name = "test_target", havingValue = "sorm")
    public TestServiceInterface getSormTestService() {
        return new SormPerformaceTestService();
    }
}

package com.ibeetl.dao.sorm.impl;

import com.ibeetl.dao.sorm.SormUserService;
import com.ibeetl.sorm.domain.SormCustomer;
import com.ibeetl.sorm.domain.SormSqlOrder;
import com.ibeetl.sorm.domain.SormUser;
import com.querydsl.sql.SQLQuery;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Select;
import org.jooq.impl.DSL;
import org.smallframework.spring.DaoTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sf.common.wrapper.Page;
import sf.database.jdbc.sql.PageStrategy;
import sf.jooq.JooqTables;
import sf.jooq.tables.JooqTable;
import sf.querydsl.QueryDSLTables;
import sf.querydsl.SQLRelationalPath;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SormUserServiceImpl implements SormUserService {

    @Resource
    private DaoTemplate dt;
    @Autowired
    DataSource ds = null;


    @Override
    public void addUser(SormUser user) {
        dt.insertFast(user);

    }

    @Override
    public SormUser unique(Integer id) {
        return dt.selectByPrimaryKeys(SormUser.class, id);
    }

    @Override
    public void updateUser(SormUser user) {
        dt.update(user);

    }

    @Override
    public void pageQuery(String code) {
//        pageQueryCommon(code);
        pageQueryObject(code);
//        pageQueryFast(code);
//        pageQueryJooq(code);
//        pageQueryQueryDSL(code);
//        pageQueryTemplate(code);

    }

    private void pageQueryObject(String code) {
        SormUser q = new SormUser();
        if (StringUtils.isNotEmpty(code)) {
            q.useQuery().createCriteria().eq(SormUser.Field.code, code);
        }
        Page<SormUser> page = dt.selectPage(q, 1, 10);
        List list = page.getList();

    }

    private void pageQueryCommon(String code) {
        StringBuilder sql = new StringBuilder("select * from sys_user where 1=1");
        if (StringUtils.isNotEmpty(code)) {
            sql.append(" and code=?");
        }

        Object[] params = null;
        if (StringUtils.isNotEmpty(code)) {
            params = new Object[]{code};
        }


        Page<SormUser> page = dt.selectPage(1, 10, SormUser.class, sql.toString(), params);

        List list = page.getList();

    }


    private void pageQueryFast(String code) {
        StringBuilder counstSql = new StringBuilder("select count(1) from sys_user where 1=1 ");
        StringBuilder sql = new StringBuilder("select * from sys_user where 1=1 ");
        if (StringUtils.isNotEmpty(code)) {
            sql.append(" and code=? ");
            counstSql.append(" and code=?");
        }

        Object[] params = null;
        if (StringUtils.isNotEmpty(code)) {
            params = new Object[]{code};
        }
        List list = null;
        sql.append("limit 1,10");
        Page<SormUser> page = dt.selectPageRaw(1, 10, SormUser.class, counstSql.toString(), params, sql.toString(), params, PageStrategy.hasOffsetLimit);

        list = page.getList();

    }

    private void pageQueryTemplate(String code) {
        Map<String, Object> p = new HashMap<>();
        p.put("code", code);
        Page<SormUser> page = dt.selectPageTemplate(0, 10, SormUser.class, "selectUserPage", p);
        List list = page.getList();

    }

    private void pageQueryJooq(String code) {
        JooqTable<?> qu = JooqTables.getTable(null, SormUser.class);
        Select<?> select = DSL.selectFrom(qu).where(qu.c(SormUser.Field.code).eq(code)).offset(1).limit(10);
        Select<?> selectCount = DSL.select(DSL.count()).from(qu).where(qu.c(SormUser.Field.code).eq(code));
        Page<SormUser> page = dt.getJooq().jooqSelectPage(selectCount, select, SormUser.class);
        List list = page.getList();
    }

    private void pageQueryQueryDSL(String code) {
        SQLRelationalPath<SormUser> qu = QueryDSLTables.relationalPathBase(null, SormUser.class);
        SQLQuery<SormUser> query = new SQLQuery<SormUser>().select(qu).from(qu).where(qu.string(SormUser.Field.code).eq(code));
        Page<SormUser> page = dt.getQueryDSL().queryDSLSelectPage(query, SormUser.class, 1, 10);
        List list = page.getList();
    }

    @Override
    public void example(Integer id) {

//		ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("id", ExampleMatcher.GenericPropertyMatchers.exact());
//
        SormUser user = new SormUser();
        user.setId(id);
//        user.useQuery().createCriteria().eq(Field.id,id);
        SormUser su = dt.selectOne(user);

//        JooqTable<?> qu = JooqTables.getTable(SormUser.class);
//        Select<?> select = DSL.selectFrom(qu).where(qu.c(Field.id).eq(id));
//        SormUser su = dt.jooqSelectOne(select, SormUser.class);

//        SQLRelationalPath<SormUser> qu = QueryDSLTables.relationalPathBase(SormUser.class);
//        SQLQuery<SormUser> query = new SQLQuery<>();
//        query.select(qu).from(qu).where(qu.number(Field.id).eq(id));
//        SormUser su = dt.queryDSLSelectOne(query,SormUser.class);

        if (su == null || su.getId() == null) {
            throw new RuntimeException("wrong result");
        }
    }

    private static SormSqlOrder q = new SormSqlOrder();

    @Override
    public void ormQuery() {
        List<SormSqlOrder> list = dt.fetchCascade(q, SormSqlOrder.class, SormSqlOrder.CascadeField.customer);
//        List<SormSqlOrder> list = dt.selectList(new SormSqlOrder());
        if (list.isEmpty()) {
            throw new RuntimeException("orm error");
        }
        for (SormSqlOrder order : list) {
//            dt.fetchLinks(order, SormSqlOrder.CascadeField.customer);
            SormCustomer customer = order.getCustomer();
            if (customer == null) {
                throw new RuntimeException("orm error");
            }
        }
    }
}

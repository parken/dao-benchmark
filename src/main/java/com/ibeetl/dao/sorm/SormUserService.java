package com.ibeetl.dao.sorm;

import com.ibeetl.sorm.domain.SormUser;

public interface SormUserService {
    void addUser(SormUser user);

    SormUser unique(Integer id);

    void updateUser(SormUser user);

    void pageQuery(String code);

    void example(Integer id);

    void ormQuery();
}

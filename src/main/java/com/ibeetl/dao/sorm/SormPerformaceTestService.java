package com.ibeetl.dao.sorm;

import com.ibeetl.dao.common.TestServiceInterface;
import com.ibeetl.sorm.domain.SormUser;
import org.springframework.beans.factory.annotation.Autowired;

public class SormPerformaceTestService implements TestServiceInterface {

	
	int index = 1;
	
	@Autowired
	SormUserService userService;
	
	@Override
	public void testAdd() {
		SormUser user = this.getNewUser();
		userService.addUser(user);

	}

	@Override
	public void testUnique() {
		userService.unique(1);

	}

	@Override
	public void testUpdateById() {
		SormUser user = this.getNewUser();
		user.setId(1);
		user.setCode("abc");
		userService.updateUser(user);

	}
	
	private SormUser getNewUser() {
		SormUser user = new SormUser();
		user.setId(index++);
		user.setCode("abc");
		return user;
		
	}

	@Override
	public void testPageQuery() {
		userService.pageQuery("abc");
		
	}

	@Override
	public void testExampleQuery() {
		userService.example(1);
		
	}

	@Override
	public void testOrmQUery() {
		userService.ormQuery();
	}
}

package com.ibeetl.dao.mybatis.service.impl;

import com.ibeetl.dao.mybatis.entity.SysUser;
import com.ibeetl.dao.mybatis.mapper.MybatisDao;
import com.ibeetl.dao.mybatis.service.MybatisUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * create by 叶云轩 at 2018/5/23-上午9:37
 * contact by tdg_yyx@foxmail.com
 */
@Service
public class MyBatisUserServiceImpl implements MybatisUserService {
    @Resource
    private MybatisDao dao;

    @Override
    public void addUser(SysUser user) {
        dao.addUser(user);
    }

    @Override
    public SysUser unique(Integer id) {
        return dao.unique(id);
    }

    @Override
    public void updateUser(SysUser user) {
        dao.updateUser(user);
    }

    @Override
    public void pageQuery(String code) {
        int count = dao.getPageCount(code);
        if (count > 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", code);
            map.put("offset", 1);
            map.put("limit", 10);
            List<SysUser> list = dao.getPageList(map);
        }
    }

    @Override
    public void example(Integer id) {
        dao.example(id);
    }
}

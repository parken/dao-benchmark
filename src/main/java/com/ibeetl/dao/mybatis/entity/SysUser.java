package com.ibeetl.dao.mybatis.entity;

import java.io.Serializable;

public class SysUser {

    private static final long serialVersionUID = -7282685248525811969L;
    private Integer id;
    private String code;

    /**
     * 指定主键
     */
    protected Serializable pkVal() {
        return this.id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

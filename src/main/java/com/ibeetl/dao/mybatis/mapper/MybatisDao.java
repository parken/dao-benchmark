package com.ibeetl.dao.mybatis.mapper;

import com.ibeetl.dao.mybatis.entity.SysUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import java.util.List;
import java.util.Map;

@Mapper
public interface MybatisDao {
    @Insert("insert into sys_user (id,code) values (#{id},#{code})")
    void addUser(SysUser user);

    @Select("select * from sys_user where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "code", column = "code")
    })
    SysUser unique(@Param("id") Integer id);

    @UpdateProvider(type = MybatisSqlUtils.class, method = "getUpdateSql")
    void updateUser(SysUser user);

    @SelectProvider(type = MybatisSqlUtils.class, method = "getPageCount")
    int getPageCount(String code);

    /**
     * String code
     * int offset
     * int limit
     * @param code
     * @param offset
     * @param limit
     * @return
     */
    @SelectProvider(type = MybatisSqlUtils.class, method = "getPageList")
    List<SysUser> getPageList(Map<String, Object> map);

    @Select("select * from sys_user where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "code", column = "code")
    })
    SysUser example(Integer id);


}
package com.ibeetl.dao.mybatis.mapper;

import com.ibeetl.dao.mybatis.entity.SysUser;

import java.util.Map;

public class MybatisSqlUtils {
    public static String getUpdateSql(SysUser user) {
        StringBuilder sb = new StringBuilder("update sys_user set code = #{code} where id = #{id}");
        return sb.toString();
    }

    public static String getPageCount(String code) {
        StringBuilder sb = new StringBuilder("select count(*) from sys_user where code = #{code}");
        return sb.toString();
    }

    public static String getPageList(Map<String, Object> map) {
        String code = (String) map.get("code");
        int offset = (int) map.get("offset");
        int limit = (int) map.get("limit");
        StringBuilder sb = new StringBuilder("select * from sys_user where code = #{code}");
        sb.append(" limit ").append(offset).append(",").append(limit);
        return sb.toString();
    }
}

package com.ibeetl.dao.jpa.entity;
import javax.persistence.*;
import java.io.Serializable;

@Entity()
@Table(name="sys_user")
public class JpaUser   implements Serializable{
	@Id
	private Integer id ;
	@Column()
	private String code ;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	

}

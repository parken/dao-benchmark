package com.ibeetl.dao.beetlsql.conf;

import org.beetl.core.Context;
import org.beetl.core.Function;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;

@Configuration
public class MyBeetlConfig {

}

class JoinFunction implements Function{

	@Override
	public Object call(Object[] paras, Context ctx) {
		Collection<?> c = (Collection<?>)paras[0];
		StringBuilder sb = new StringBuilder();
		for(Object o:c) {
			if(o instanceof String) {
				sb.append("\"").append((String)o).append("\",");
			}else {
				sb.append(o).append(",");
			}
		}
		sb.setLength(sb.length()-1);
		return sb.toString();
	}
	
}

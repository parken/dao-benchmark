package com.ibeetl.dao.beetlsql.entity;

import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.fetch.annotation.Fetch;
import org.beetl.sql.fetch.annotation.FetchSql;

@Table(name = "sys_order")
@Fetch(level =2)
public class BeetlSqlOrder extends TailBean {
	@AssignID
	private Integer id;
	private String name;
	private Integer custId;
	@FetchSql("select * from sys_customer where id =#{custId}")
	private BeetlSqlCustomer customer;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCustId() {
		return custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public BeetlSqlCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(BeetlSqlCustomer customer) {
		this.customer = customer;
	}
}
